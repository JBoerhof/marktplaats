<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisement_status_advertisement', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('advertisement_id');
            $table->unsignedBigInteger('status_advertisement_id');
            $table->timestamps();
            
            $table->foreign('advertisement_id')->references('id')->on('advertisements');
            $table->foreign('status_advertisement_id', 'status_adv_FK')->references('id')->on('status_advertisements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisement_status_advertisement');
    }
}
