<?php

use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offers')->insert([
            'user_id' => '1',
            'advertisement_id' => '1',
            'bod' => '50.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('offers')->insert([
            'user_id' => '1',
            'advertisement_id' => '2',
            'bod' => '50.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('offers')->insert([
            'user_id' => '1',
            'advertisement_id' => '3',
            'bod' => '50.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('offers')->insert([
            'user_id' => '1',
            'advertisement_id' => '4',
            'bod' => '50.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('offers')->insert([
            'user_id' => '1',
            'advertisement_id' => '5',
            'bod' => '50.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
