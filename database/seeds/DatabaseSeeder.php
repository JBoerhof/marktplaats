<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(ConditionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AdvertisementTableSeeder::class);
        $this->call(ImageTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(SubCategoryTableSeeder::class);
        $this->call(StatusAdvertisementTableSeeder::class);
        $this->call(OffersTableSeeder::class);
        $this->call(Advertisement_StatusTableSeeder::class);
        $this->call(Advertisement_SubcategoryTableSeeder::class);
    }
}
