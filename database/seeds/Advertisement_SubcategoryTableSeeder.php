<?php

use Illuminate\Database\Seeder;

class Advertisement_SubcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisement_sub_category')->insert([
            'sub_category_id' => '2',
            'advertisement_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_sub_category')->insert([
            'sub_category_id' => '4',
            'advertisement_id' => '2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_sub_category')->insert([
            'sub_category_id' => '5',
            'advertisement_id' => '3',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_sub_category')->insert([
            'sub_category_id' => '6',
            'advertisement_id' => '4',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_sub_category')->insert([
            'sub_category_id' => '3',
            'advertisement_id' => '5',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
