<?php

use Illuminate\Database\Seeder;

class Advertisement_StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisement_status_advertisement')->insert([
            'advertisement_id' => '1',
            'status_advertisement_id' => '4',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_status_advertisement')->insert([
            'advertisement_id' => '1',
            'status_advertisement_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_status_advertisement')->insert([
            'advertisement_id' => '2',
            'status_advertisement_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_status_advertisement')->insert([
            'advertisement_id' => '3',
            'status_advertisement_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_status_advertisement')->insert([
            'advertisement_id' => '4',
            'status_advertisement_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisement_status_advertisement')->insert([
            'advertisement_id' => '5',
            'status_advertisement_id' => '2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
