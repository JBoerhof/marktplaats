<?php

use Illuminate\Database\Seeder;

class SubCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_categories')->insert([
            'category_id' => '1',
            'name' => 'Hardeschijven',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]); 
        DB::table('sub_categories')->insert([
            'category_id' => '1',
            'name' => 'Videokaarten',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]); 
        DB::table('sub_categories')->insert([
            'category_id' => '1',
            'name' => 'Moederborden',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]); 
        DB::table('sub_categories')->insert([
            'category_id' => '1',
            'name' => 'Beeldschermen',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]); 
        DB::table('sub_categories')->insert([
            'category_id' => '2',
            'name' => 'Gereedschappen',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]); 
        DB::table('sub_categories')->insert([
            'category_id' => '2',
            'name' => 'Planten',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")

        ]); 
    }
}
