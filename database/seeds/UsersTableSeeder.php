<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'username' => 'JasperB',
            'name' => 'Jasper',
            'surname' => 'Boerhof',
            'adres' => 'Plataanlaan',
            'housenumber' => '6',
            'postcode' => '9771AX',
            'city' => 'Sauwerd',
            'email' => 'jasperboerhof@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
