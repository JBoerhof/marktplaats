<?php

use Illuminate\Database\Seeder;

class AdvertisementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisements')->insert([
            'user_id' => '1',
            'condition_id' => '1',
            'title' => 'Nieuwe videokaart',
            'discription' => 'lorem ipdum dollar',
            'askingprice' => '150.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisements')->insert([
            'user_id' => '1',
            'condition_id' => '3',
            'title' => 'Gebruikte beeldscherm',
            'discription' => 'lorem ipdum dollar',
            'askingprice' => '250.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisements')->insert([
            'user_id' => '1',
            'condition_id' => '3',
            'title' => 'Oude schep',
            'discription' => 'lorem ipdum dollar',
            'askingprice' => '60.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisements')->insert([
            'user_id' => '1',
            'condition_id' => '1',
            'title' => 'Yuka',
            'discription' => 'lorem ipdum dollar',
            'askingprice' => '80.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('advertisements')->insert([
            'user_id' => '1',
            'condition_id' => '1',
            'title' => 'Nieuw Moederbord',
            'discription' => 'lorem ipdum dollar',
            'askingprice' => '650.00',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
