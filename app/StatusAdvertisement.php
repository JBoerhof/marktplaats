<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusAdvertisement extends Model
{
    public function advertisement()
    {
        return $this->belongsToMany('App\Advertisement');
    }
}
