<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $with = ['user'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function advertisement()
    {
        return $this->belongsTo('App\Advertisement');
    }

    protected $fillable = [
        'user_id', 'advertisement_id', 'bod'
    ];
}
