<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Advertisements_Status extends Pivot
{
    protected $fillable = [
        'advertisement_id', 'status_id'
    ];
}
