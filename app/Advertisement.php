<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $with = ['subcategory', 'status', 'offer', 'image', 'condition', 'user.role'];
    
    public function subcategory()
    {
        return $this->belongsToMany('App\SubCategory');
    }

    public function status()
    {
        return $this->belongsToMany('App\StatusAdvertisement');
    }

    public function condition()
    {
        return $this->belongsTo('App\Condition');
    }

    public function image()
    {
        return $this->hasMany('App\Image');
    } 

    public function offer()
    {
        return $this->hasMany('App\Offer');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }      

    protected $fillable = [
        'user_id', 'condition_id', 'title', 'discription', 'askingprice',
    ];
}
