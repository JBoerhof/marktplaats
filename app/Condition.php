<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    public function advertisement()
    {
        return $this->hasMany('App\Advertisement');
    }
}
