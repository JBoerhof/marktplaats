<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Advertisements_Subcategory extends Pivot
{
    protected $fillable = [
        'sub_category_id', 'advertisement_id'
    ];
}
