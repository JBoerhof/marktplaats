<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function advertisements()
    {
        return $this->belongsToMany('App\Advertisement');
    }

    protected $fillable = [
        'category_id', 'name'
    ];
}
