<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AdvertisementResource as AdvertisementResource;

class SubCategoryResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'name' => $this->name,
            'advertisements' => AdvertisementResource::collection($this->advertisements),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            //'' => $this->,
        ];
        //parent::toArray($request);
    }
}
