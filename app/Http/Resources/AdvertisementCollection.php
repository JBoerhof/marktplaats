<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\AdvertisementResource as AdvertisementResource;

class AdvertisementCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // dd($request);
        
        return [
            'data' => AdvertisementResource::collection($this->collection),
            
            // 'count' => $this->count(),
            // 'total' => $this->total(),
            // 'prev'  => $this->previousPageUrl(),
            // 'next'  => $this->nextPageUrl(),
            // 'current_page'  => $this->currentPage(),
            // 'first_page_url'  => $this->firstPageUrl(),
            // 'from'  => $this->from(),
            // 'last_page'  => $this->lastPage(),
            // 'last_page_url'  => $this->lastPageUrl(),
            // 'next_page_url'  => $this->nextPageUrl(),
            // 'path'  => $this->path(),
            // 'per_page'  => $this->perPage(),
            
        ]; 
    }
}
