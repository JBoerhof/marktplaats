<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdvertisementResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'discription' => $this->discription,
            'askingprice' => $this->askingprice,
            'image' => $this->image->first(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            //'' => $this->,
        ];
        //parent::toArray($request);
    }
}
