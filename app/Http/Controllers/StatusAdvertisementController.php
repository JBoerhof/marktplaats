<?php

namespace App\Http\Controllers;

use App\StatusAdvertisement;
use Illuminate\Http\Request;

class StatusAdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'statusadvertisements' => StatusAdvertisement::All(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        // return response()->json([
        //     'statusadvertisements' => StatusAdvertisement::All(),
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StatusAdvertisements  $statusAdvertisements
     * @return \Illuminate\Http\Response
     */
    public function show(StatusAdvertisements $statusAdvertisements)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StatusAdvertisements  $statusAdvertisements
     * @return \Illuminate\Http\Response
     */
    public function edit(StatusAdvertisements $statusAdvertisements)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StatusAdvertisements  $statusAdvertisements
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusAdvertisements $statusAdvertisements)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StatusAdvertisements  $statusAdvertisements
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusAdvertisements $statusAdvertisements)
    {
        //
    }
}
