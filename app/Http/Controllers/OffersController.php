<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Advertisement;
use Illuminate\Http\Request;

class OffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $advertisement = Advertisement::find($request->post('advertisement_id'));
        
        if($advertisement->offer()->orderBy('bod', 'desc')->first()['bod'] >= $request->post('bod')){
            return response()->json([
                'advertisement' => $advertisement, 'message' => ['state' => 'danger', 'message' => 'Your offer is to low.']
            ]);
        } else {
            $data = Offer::create($request->post());
            $advertisement = Advertisement::find($request->post('advertisement_id'));
            return response()->json([
                'advertisement' => $advertisement, 'message' => ['state' => 'success', 'message' => 'Your offer is placed!']
            ]);
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        //
    }
}
