<?php

namespace App\Http\Controllers;

use App\Advertisement_Status;
use Illuminate\Http\Request;

class PivotAdvertisementStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advertisement_Status  $advertisement_status
     * @return \Illuminate\Http\Response
     */
    public function show(Advertisement_Status $advertisement_status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advertisement_Status  $advertisement_status
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement_Status $advertisement_status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advertisement_Status  $advertisement_status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisement_Status $advertisement_status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advertisement_Status  $advertisement_status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertisement_Status $advertisement_status)
    {
        //
    }
}
