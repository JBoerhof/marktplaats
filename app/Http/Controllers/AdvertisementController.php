<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Image;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as ModImage;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return response()->json([
        //     'advertisements' => Advertisement::All(),
        // ]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Advertisement::create($request->post());
        
        $advertisement = Advertisement::orderBy('created_at', 'desc')->first();
        
        foreach($request->file('images') as $image){
            $filename = $image->store('public');
            $image = new Image(['name' => basename($filename)]);

            $advertisement->image()->save($image);
            
            $img = ModImage::make('storage/'.$image['name'])->resize(300, null,  function ($constraint) {
                $constraint->aspectRatio();
            })->save('storage/normal/'.$image['name']);
            $img = ModImage::make('storage/'.$image['name'])->resize(50, null,  function ($constraint) {
                $constraint->aspectRatio();
            })->save('storage/thumb/'.$image['name']);
        }        
        
        $advertisement->subcategory()->attach($request->post()['sub_category_id']);
        $advertisement->status()->attach($request->post()['status_advertisement_id']);
        
        return response()->json([
            'advertisement' => $advertisement
        ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function show(Advertisement $advertisement)
    {
        return response()->json([
            'advertisement' => $advertisement,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisement $advertisement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertisement $advertisement)
    {
        //
    }
}
