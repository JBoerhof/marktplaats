<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function advertisement()
    {
        return $this->belongsTo('App\Advertisement');
    }

    protected $fillable = [
        'advertisement_id', 'name'
    ];
}
