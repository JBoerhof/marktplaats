import Vue from 'vue';
import VueRouter from 'vue-router';
import Axios from 'Axios';
import store from './store'

window.Vue = Vue;

window.axios = Axios;

window.axios.interceptors.response.use(response=>{
    if (response.data.message) store.commit('SET_MESSAGE', response.data.message)
    return response
}, error => console.error(error))

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';