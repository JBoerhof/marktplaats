import Vue from "vue";
import Vuex from "vuex";
import Router from './routes';

Vue.use(Vuex);
// Vue.use(Router)

export default new Vuex.Store({
    state: {
      homeadverts: [],
      categories: [],
      subcategories: [],
      advertisements: [],
      conditions: [],
      statusadvertisements: [],
      message: []
    },

    getters: {
      getHomeAdverts: state => state.homeadverts,
      getCategories: state => state.categories,
      getSubCategories: state => state.subcategories,
      getAdvertisements: state => state.advertisements,
      getConditions: state => state.conditions,
      getStatusAdvertisements: state => state.statusadvertisements,
      getMessage: state => state.message
    },

    mutations: {
      SET_HOMEADVERTS(state, payload) {
        state.homeadverts = payload;
      },

      SET_CATEGORIES(state, payload) {
        state.categories = payload;
      },

      SET_SUBCATEGORIES(state, payload) {
        state.subcategories = payload;
      },

      SET_ADVERTISEMENTS(state, payload) {
        state.advertisements = payload;
      },

      SET_CONDITIONS(state, payload) {
        state.conditions = payload;
      },

      SET_STATUSADVERTISEMENTS(state, payload) {
        state.statusadvertisements = payload;
      },

      SET_MESSAGE(state, payload) {
        state.message = payload;
      }
    },

    actions: {
      setHomeAdverts(context) {
        axios.get("/api/homeadverts").then(response => {
          context.commit("SET_HOMEADVERTS", response.data.homeadverts);
        });
      },

      setCategories(context) {
        axios.get("/api/category").then(response => {
          context.commit("SET_CATEGORIES", response.data.categories);
        });
      },

      setSubCategories(context, subcategoryId) {        
        axios.get(`/api/subcategory/${subcategoryId}`).then(response => {
          context.commit("SET_SUBCATEGORIES", response.data.subcategory);
        });
      },

      setAdvertisements(context, advertisementId) {
        axios.get(`/api/advertisement/${advertisementId}`).then(response => {
          context.commit("SET_ADVERTISEMENTS", response.data.advertisement);
        })
      },

      setConditions(context) {
        axios.get(`/api/condition/`).then(response => {
          context.commit("SET_CONDITIONS", response.data.conditions);
        })
      },

      setStatusAdvertisements(context) {
        axios.get(`/api/statusadvertisement/`).then(response => {
          context.commit("SET_STATUSADVERTISEMENTS", response.data.statusadvertisements);
        })
      },

      CreateAdvertisement(context, CreateAdvertisement) {
        axios.post(`/api/advertisement`, CreateAdvertisement).then(response => {
          context.commit("SET_ADVERTISEMENTS", response.data.advertisement);
          Router.push({ path: `/advertisement/${response.data.advertisement.id}` })
        }).catch(function (error) {
          console.log(error);
        })
      },

      CreateOffer(context, CreateOffer) {
        axios.post(`/api/offer`, CreateOffer).then(response => {
          context.commit("SET_ADVERTISEMENTS", response.data.advertisement);
          Router.push({ path: `/advertisement/${response.data.advertisement.id}` })
        }).catch(function (error) {
          console.log(error);
        })
      },

    }
})