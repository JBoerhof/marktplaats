import './bootstrap';
import router from './routes';
import App from './layouts/App';
import store from "./store";
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Lingallery from 'lingallery';

Vue.component('lingallery', Lingallery);
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.use(BootstrapVue)

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
