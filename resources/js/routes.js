import VueRouter from 'vue-router';
import Vue from 'vue'

import Nav from './layouts/nav'
import Header from './layouts/header'
// import breadcrumbs from './layouts/breadcrumbs'
import Home from './views/home'
import Category from './views/category'
import CategoryShow from './views/category/show'
import subCategoryShow from './views/subcategory/show'
import AdvertisementShow from './views/advertisement/show'
import AdvertisementCreate from './views/advertisement/create'

Vue.use(VueRouter);

let routes = [
    {
        path: '',
        name: 'Home',
        components: {
            header: Header,
            nav: Nav,
            default: Home,
            // breadcrumbs: breadcrumbs
        }
    },
    {
        path: '/advertisement/create',
        components: {
            header: Header,
            nav: Nav,
            default: AdvertisementCreate,
            // breadcrumbs: breadcrumbs
        }
    },
    {
        path: '/category',
        components: {
            header: Header,
            nav: Nav,
            default: Category,
            // breadcrumbs: breadcrumbs
        }
    },
    {
        name: 'CategoryShow',
        path: '/category/:id',
        components: { 
            header: Header,
            nav: Nav,
            default: CategoryShow,
            // breadcrumbs: breadcrumbs
        }
    },
    {
        name: 'subCategoryShow',
        path: '/subcategory/:id',
        components: { 
            header: Header,
            nav: Nav,
            default: subCategoryShow,
            // breadcrumbs: breadcrumbs
        }
    },
    {
        name: 'AdvertisementShow',
        path: '/advertisement/:id',
        components: { 
            header: Header,
            nav: Nav,
            default: AdvertisementShow,
            // breadcrumbs: breadcrumbs
        }
    },

]

export default new VueRouter({
    mode: 'history',
    routes
})